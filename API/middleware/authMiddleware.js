const jwt = require('jsonwebtoken');

const secretKey = 'your-secret-key'; // Same secret key used for token generation

// Middleware to verify the access token
function verifyAccessToken(req, res, next) {
  const accessToken = req.headers.authorization?.split(' ')[1];

  if (!accessToken) {
    return res.status(401).json({ message: 'Access token is missing' });
  }

  jwt.verify(accessToken, secretKey, (err, user) => {
    if (err) {
      return res.status(403).json({ message: 'Invalid access token' });
    }

    req.user = user;
    next();
  });
}

module.exports = {
  verifyAccessToken,
};
