const jwt = require('jsonwebtoken');

const secretKey = 'secret'; // Change this to a strong secret key
const accessTokenExpiration = '15m'; // Access token expiration time
const refreshTokenExpiration = '7d'; // Refresh token expiration time

// Sample user data (Replace with a real user database)
const users = [
  { id: 1, username: 'user1', password: 'password1' },
  { id: 2, username: 'user2', password: 'password2' },
];

// Function to generate an access token
function generateAccessToken(user) {
  return jwt.sign({ userId: user.id }, secretKey, { expiresIn: accessTokenExpiration });
}

// Function to generate a refresh token
function generateRefreshToken(user) {
  return jwt.sign({ userId: user.id }, secretKey, { expiresIn: refreshTokenExpiration });
}

// Authenticate user and generate tokens
function login(req, res) {
  const { username, password } = req.body;

  // Replace this with actual user authentication logic
  const user = users.find((u) => u.username === username && u.password === password);

  if (!user) {
    return res.status(401).json({ message: 'Authentication failed' });
  }

  const accessToken = generateAccessToken(user);
  const refreshToken = generateRefreshToken(user);

  res.json({ accessToken, refreshToken });
}

module.exports = {
  login,
};
