function solution(a, m, k) {
  let count = 0; // Inisialisasi variabel untuk menghitung jumlah subarray yang memenuhi syarat
  let sum = 0; // Inisialisasi variabel untuk menyimpan jumlah elemen dalam jendela geser
  let left = 0; // Pointer untuk elemen di sebelah kiri jendela geser

  for (let right = 0; right < a.length; right++) { // Looping melalui array a
    sum += a[right]; // Tambahkan elemen saat ini ke jumlah

    if (right - left + 1 === m) { // Jika panjang jendela geser mencapai m
      if (sum === k) { // Jika jumlah elemen dalam jendela geser sama dengan k
        count++; // Tambahkan ke jumlah subarray yang memenuhi syarat
      }
      sum -= a[left]; // Kurangi elemen di kiri jendela geser
      left++; // Geser jendela ke kanan
    }
  }

  return count; // Kembalikan jumlah subarray yang memenuhi syarat
}

// Contoh penggunaan:
const a1 = [2, 4, 7, 5, 3, 5, 8, 5, 1, 7];
const m1 = 4;
const k1 = 10;
console.log(`Input: a = [${a1}], m = ${m1}, k = ${k1}`);
console.log(`Output: ${solution(a1, m1, k1)}`);
// Output yang diharapkan: 5 (penjelasan dalam komentar di atas)

const a2 = [15, 8, 8, 2, 6, 4, 1, 7];
const m2 = 2;
const k2 = 8;
console.log(`Input: a = [${a2}], m = ${m2}, k = ${k2}`);
console.log(`Output: ${solution(a2, m2, k2)}`);
// Output yang diharapkan: 2 (penjelasan dalam komentar di atas)
