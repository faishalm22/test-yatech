# Authentication API Documentation

## Login

Authenticate a user and obtain access and refresh tokens.

- **URL**: `/auth/login`
- **Method**: `POST`
- **Request Body**:

  | Parameter  | Type     | Description         |
  |------------|----------|---------------------|
  | `username` | string   | User's username.    |
  | `password` | string   | User's password.    |

- **Responses**:

  - **200 OK**
    - **Description**: Authentication successful.
    - **Response Body**:

      ```json
      {
        "accessToken": "your-access-token",
        "refreshToken": "your-refresh-token"
      }
      ```

  - **401 Unauthorized**
    - **Description**: Authentication failed. Invalid username or password.
    - **Response Body**:

      ```json
      {
        "message": "Authentication failed"
      }
      ```

- **Example**:

  ```bash
  curl -X POST http://localhost:3000/auth/login -d "username=user1&password=password1"
  ```

  - **Response**:

    ```json
    {
      "accessToken": "your-access-token",
      "refreshToken": "your-refresh-token"
    }
    ```

### Error Handling

- If the request body is missing `username` or `password`, a `400 Bad Request` response is returned with a message indicating the missing fields.

- If the authentication fails due to an incorrect username or password, a `401 Unauthorized` response is returned with a "Authentication failed" message.

- Other error scenarios should be handled and documented as needed for your specific implementation.

Pastikan untuk mengganti `your-access-token` dan `your-refresh-token` dengan nilai sesungguhnya yang dihasilkan oleh aplikasi Anda saat melakukan autentikasi. Juga, pastikan untuk menyertakan semua error handling yang sesuai untuk kasus-kasus yang mungkin terjadi dalam aplikasi Anda.
